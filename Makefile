build: format
	env GO111MODULE=on CGO_ENABLED=0 GOOS=linux go build -ldflags="-s -w" -o ./bin/sitemap sitemap/cmd/main.go

tidy:
	go mod tidy

format:
	gofmt -w -l ./sitemap/

import:
	goimports -e -d -w ./sitemap/

inspect:
	cat -e -t -v Makefile

docker-test:
	@docker run -it --rm -v $$PWD:/app -v $$GOPATH/pkg/mod/cache:/go/pkg/mod/cache -w /app golang:1.14 make test

depth=2
docker-run:
	@docker run -it --rm -v $$PWD:/app -v $$GOPATH/pkg/mod/cache:/go/pkg/mod/cache -w /app golang:1.14 go run sitemap/cmd/main.go -domain $(domain) -depth $(depth)

test:
	go test -v -race ./...
