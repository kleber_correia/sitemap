FROM golang:1.14 as build
WORKDIR /go/sitemap
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN make build

FROM alpine:3.9
WORKDIR /usr/bin

COPY --from=build /go/sitemap/bin/sitemap .
CMD sitemap
