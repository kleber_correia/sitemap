module github.com/klebervirgilio/sitemap

go 1.14

require (
	github.com/andreyvit/diff v0.0.0-20170406064948-c7f18ee00883
	github.com/antchfx/htmlquery v1.2.3
	github.com/sergi/go-diff v1.1.0 // indirect
	github.com/xlab/treeprint v1.0.0
	golang.org/x/net v0.0.0-20200421231249-e086a090c8fd
)
