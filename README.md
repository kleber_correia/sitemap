### Sitemap Generator

This is project implements a web crawler limited to one domain. It crawls all pages within a given domain, only following domain links. Given a URL, it outputs a site map, showing which static assets each page depends on.

### Goals

* ✅ Store pages/links in a tree structure, in order to make my life easier when outputting the sitemap.
* ✅ Consume a given page/link only once.
* ✅ Put in place a back-pressure mechanism.

### Initial thoughts

It's fair to assume the challenge has at least a main "entity" `Page` which will likely have many **followable** `Links`.

Roughly sketching the main components:

1. ✅ PageFetcher - fetches HTML blobs via `http.Get`.
2. ✅ LinkFilter - finds followable `Links`.
3. ✅ PageParser - parses HTML blob, translating it into a workable structure.
4. ✅ LinkConsumer
    - ~~Enqueue followable children~~
    - Persists sitemap in the repository
5. ✅ Repository - is sitemap hierarchic keeper and it holds the sitemap data
    *MemoryRepo* - Implements a simple tree in order to preserve sitemap hierarchic

### With more time I could...

After spending few hours (with way to many distractions, I've got 2 kids) on this project, towards the end I took a more pragmatic approach in order to have a workable deliverable.

🤦‍♂️ **Test coverage** - I'm not happy with the test coverage, I focused on testing the most complex components though.

**The way links are marked as visited** - It's a very very naive implementation and it smells like a memory leak. There're definitively better ways to approach it.

**Git commits** I often advocate for well defined commits and decent explanatory messages, buuuttt...

**There's useful logging system**  Would be nice to have a structured logging mechanism respecting at least the following log levels: debug, info, warning, and error.

**panics everywhere** You will see a few `panics` in the code, I like the `fail fast` approach while developing.

**SOLID** Some components are doing too much. e.g. MemoryRepo

### Running the Sitemap generator

#### Local

```sh
# run tests
make test

# run generates sitemap
go run sitemap/cmd/main.go -domain https://www.cuvva.com -depth 3
```
#### Docker

```sh
# run tests
make docker-test

# run generates sitemap
make docker-run domain="https://www.cuvva.com" depth=2
```

### Output Sample

```
.
└── https://www.cuvva.com
    ├── Images
    │   ├── /static/images/cuvva_logo.svg
    │   ├── /static/images/any/header-home-trip.svg
    │   ├── /static/images/any/flat-iphone.png
    │   ├── /static/images/landing/comprehensive.svg
    │   ├── /static/images/perks/volunteer-day.svg
    │   ├── /static/images/landing/sanctus.svg
    │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    ├── Scripts
    │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   └── /website.b2f26761.js
    ├── Stylesheets
    │   └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── /static/design-system/illustrations/dog-says-hi.svg
    │   │   ├── /static/design-system/illustrations/new-car.svg
    │   │   ├── /static/design-system/illustrations/new-car.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   ├── Stylesheets
    │   │   └── https://use.typekit.net/xcc2pjd.css
    │   └── /car-insurance/temporary-van-insurance
    │       ├── Images
    │       │   ├── /static/images/cuvva_logo.svg
    │       │   ├── /static/images/phones/vanphone.svg
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │       │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │       ├── Scripts
    │       │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │       │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │       │   └── /website.b2f26761.js
    │       └── Stylesheets
    │           └── https://use.typekit.net/xcc2pjd.css
    ├── /car-insurance/learner-driver
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── /static/images/landing/comprehensive.svg
    │   │   ├── /static/images/landing/clock.svg
    │   │   ├── /static/images/any/carincircle.svg
    │   │   ├── /static/images/phones/learnerphone.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   ├── Stylesheets
    │   │   └── https://use.typekit.net/xcc2pjd.css
    │   └── https://www.cuvva.com/car-insurance/subscription
    │       ├── Images
    │       │   ├── /static/images/cuvva_logo.svg
    │       │   ├── /static/images/any/paymonthly.svg
    │       │   ├── /static/images/any/nohiddenfees.svg
    │       │   ├── /static/images/any/cancelanytime.svg
    │       │   ├── /static/images/any/nohiddenfees.svg
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │       │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │       ├── Scripts
    │       │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │       │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │       │   └── /website.b2f26761.js
    │       ├── Stylesheets
    │       │   └── https://use.typekit.net/xcc2pjd.css
    │       └── https://www.cuvva.com/car-insurance/temporary
    │           ├── Images
    │           │   ├── /static/images/cuvva_logo.svg
    │           │   ├── /static/images/landing/peoplewalking.svg
    │           │   ├── /static/images/landing/clock.svg
    │           │   ├── /static/images/landing/comprehensive.svg
    │           │   ├── /static/images/landing/sanctus.svg
    │           │   ├── /static/images/landing/hr.svg
    │           │   ├── /static/images/landing/phone.png
    │           │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │           │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │           │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │           │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │           │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │           ├── Scripts
    │           │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │           │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │           │   └── /website.b2f26761.js
    │           ├── Stylesheets
    │           │   └── https://use.typekit.net/xcc2pjd.css
    │           └── /
    │               ├── Images
    │               │   ├── /static/images/cuvva_logo.svg
    │               │   ├── /static/images/any/header-home-trip.svg
    │               │   ├── /static/images/any/flat-iphone.png
    │               │   ├── /static/images/landing/comprehensive.svg
    │               │   ├── /static/images/perks/volunteer-day.svg
    │               │   ├── /static/images/landing/sanctus.svg
    │               │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │               │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │               │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │               │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │               │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │               ├── Scripts
    │               │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │               │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │               │   └── /website.b2f26761.js
    │               └── Stylesheets
    │                   └── https://use.typekit.net/xcc2pjd.css
    ├── /get-an-estimate
    │   ├── Images
    │   │   └── /static/images/cuvva_logo.svg
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /free-car-checker
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── /static/design-system/illustrations/new-car.svg
    │   │   ├── /static/design-system/illustrations/man-happy-car.svg
    │   │   ├── /static/design-system/illustrations/borrowing.svg
    │   │   ├── /static/design-system/illustrations/learner.svg
    │   │   ├── /static/design-system/illustrations/van.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   ├── Stylesheets
    │   │   └── https://use.typekit.net/xcc2pjd.css
    │   └── /how-insurance-works/insurance-groups-explained
    │       ├── Images
    │       │   ├── /static/images/cuvva_logo.svg
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │       │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │       ├── Scripts
    │       │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │       │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │       │   └── /website.b2f26761.js
    │       ├── Stylesheets
    │       │   └── https://use.typekit.net/xcc2pjd.css
    │       └── /insurance-groups
    │           ├── Images
    │           │   ├── /static/images/cuvva_logo.svg
    │           │   ├── /static/design-system/illustrations/new-car.svg
    │           │   ├── /static/design-system/illustrations/man-happy-car.svg
    │           │   ├── /static/design-system/illustrations/borrowing.svg
    │           │   ├── /static/design-system/illustrations/learner.svg
    │           │   ├── /static/design-system/illustrations/van.svg
    │           │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │           │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │           │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │           │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │           │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │           ├── Scripts
    │           │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │           │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │           │   └── /website.b2f26761.js
    │           └── Stylesheets
    │               └── https://use.typekit.net/xcc2pjd.css
    ├── /support/contacting-support
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   ├── Stylesheets
    │   │   └── https://use.typekit.net/xcc2pjd.css
    │   └── /support
    │       ├── Images
    │       │   ├── /static/images/cuvva_logo.svg
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │       │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │       ├── Scripts
    │       │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │       │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │       │   └── /website.b2f26761.js
    │       ├── Stylesheets
    │       │   └── https://use.typekit.net/xcc2pjd.css
    │       └── /support/cuvva-cookie-policy
    │           ├── Images
    │           │   ├── /static/images/cuvva_logo.svg
    │           │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │           │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │           │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │           │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │           │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │           ├── Scripts
    │           │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │           │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │           │   └── /website.b2f26761.js
    │           ├── Stylesheets
    │           │   └── https://use.typekit.net/xcc2pjd.css
    │           └── /news
    │               ├── Images
    │               │   ├── /static/images/cuvva_logo.svg
    │               │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Graph-4-no-title.jpg
    │               │   ├──
    │               │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Insights-.png
    │               │   ├──
    │               │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/food-bank-header.png
    │               │   ├──
    │               │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │               │   ├──
    │               │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │               │   ├──
    │               │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │               │   ├──
    │               │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Insights-.png
    │               │   ├──
    │               │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │               │   ├──
    │               │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │               │   ├──
    │               │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │               │   ├──
    │               │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │               │   ├──
    │               │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Product-.png
    │               │   ├──
    │               │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │               │   ├──
    │               │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Product-.png
    │               │   ├──
    │               │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │               │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │               │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │               │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │               │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │               ├── Scripts
    │               │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │               │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │               │   └── /website.b2f26761.js
    │               ├── Stylesheets
    │               │   └── https://use.typekit.net/xcc2pjd.css
    │               └── /refer-a-friend
    │                   ├── Images
    │                   │   ├── /static/images/cuvva_logo.svg
    │                   │   ├── /static/images/counter-3-confetti/desktop-counter.svg
    │                   │   ├── /static/images/counter-3-confetti/mobile-counter.svg
    │                   │   ├── /static/design-system/illustrations/man-happy-car.svg
    │                   │   ├── /static/design-system/illustrations/borrowing.svg
    │                   │   ├── /static/design-system/illustrations/learner.svg
    │                   │   ├── /static/design-system/illustrations/van.svg
    │                   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │                   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │                   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │                   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │                   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │                   ├── Scripts
    │                   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │                   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │                   │   └── /website.b2f26761.js
    │                   └── Stylesheets
    │                       └── https://use.typekit.net/xcc2pjd.css
    ├── https://www.cuvva.com/car-insurance/learner-driver
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── /static/images/landing/comprehensive.svg
    │   │   ├── /static/images/landing/clock.svg
    │   │   ├── /static/images/any/carincircle.svg
    │   │   ├── /static/images/phones/learnerphone.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /car-insurance/subscription
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── /static/images/any/paymonthly.svg
    │   │   ├── /static/images/any/nohiddenfees.svg
    │   │   ├── /static/images/any/cancelanytime.svg
    │   │   ├── /static/images/any/nohiddenfees.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /support/cuvvas-terms-conditions
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   ├── Stylesheets
    │   │   └── https://use.typekit.net/xcc2pjd.css
    │   └── /support/cuvva-privacy-policy
    │       ├── Images
    │       │   ├── /static/images/cuvva_logo.svg
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │       │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │       ├── Scripts
    │       │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │       │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │       │   └── /website.b2f26761.js
    │       └── Stylesheets
    │           └── https://use.typekit.net/xcc2pjd.css
    ├── /car-insurance/temporary
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── /static/images/landing/peoplewalking.svg
    │   │   ├── /static/images/landing/clock.svg
    │   │   ├── /static/images/landing/comprehensive.svg
    │   │   ├── /static/images/landing/sanctus.svg
    │   │   ├── /static/images/landing/hr.svg
    │   │   ├── /static/images/landing/phone.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /careers
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── /static/images/listening-team/grace.png
    │   │   ├── /static/images/listening-team/anna.png
    │   │   ├── /static/images/listening-team/blake.png
    │   │   ├── /static/images/listening-team/glassdoor-stars.svg
    │   │   ├── /static/images/office-vibes/long-1.jpeg
    │   │   ├── /static/images/office-vibes/small-2.jpeg
    │   │   ├── /static/images/office-vibes/small-3.jpeg
    │   │   ├── /static/images/office-vibes/short-1.jpeg
    │   │   ├── /static/images/perks/personal-development.svg
    │   │   ├── /static/images/perks/wellbeing-budget.svg
    │   │   ├── /static/images/perks/cycle-to-work.svg
    │   │   ├── /static/images/perks/season-ticket-loan.svg
    │   │   ├── /static/images/perks/pension.svg
    │   │   ├── /static/images/perks/referral-bonus.svg
    │   │   ├── /static/images/perks/volunteer-day.svg
    │   │   ├── /static/images/perks/free-fruit-breakfast.svg
    │   │   ├── /static/images/perks/nights-out-lunches.svg
    │   │   ├── /static/images/perks/macbook.svg
    │   │   ├── /static/images/perks/wfh.svg
    │   │   ├── /static/images/perks/office-library.svg
    │   │   ├── /static/images/perks/sanctus.svg
    │   │   ├── /static/images/perks/coffee-beer-soft-drinks.svg
    │   │   ├── /static/images/perks/dog-friendly.svg
    │   │   ├── https://content.cuvva.com/segments/39d789834ebeba3553a8aa133e5735d31a2796a84b87df9e08a59fe53cf12b86/assets/product.svg
    │   │   ├── https://content.cuvva.com/segments/39d789834ebeba3553a8aa133e5735d31a2796a84b87df9e08a59fe53cf12b86/assets/product.svg
    │   │   ├── https://content.cuvva.com/segments/39d789834ebeba3553a8aa133e5735d31a2796a84b87df9e08a59fe53cf12b86/assets/product.svg
    │   │   ├── https://content.cuvva.com/segments/39d789834ebeba3553a8aa133e5735d31a2796a84b87df9e08a59fe53cf12b86/assets/product.svg
    │   │   ├── https://content.cuvva.com/segments/39d789834ebeba3553a8aa133e5735d31a2796a84b87df9e08a59fe53cf12b86/assets/default.svg
    │   │   ├── https://content.cuvva.com/segments/39d789834ebeba3553a8aa133e5735d31a2796a84b87df9e08a59fe53cf12b86/assets/default.svg
    │   │   ├── https://content.cuvva.com/segments/39d789834ebeba3553a8aa133e5735d31a2796a84b87df9e08a59fe53cf12b86/assets/default.svg
    │   │   ├── https://content.cuvva.com/segments/39d789834ebeba3553a8aa133e5735d31a2796a84b87df9e08a59fe53cf12b86/assets/default.svg
    │   │   ├── https://content.cuvva.com/segments/39d789834ebeba3553a8aa133e5735d31a2796a84b87df9e08a59fe53cf12b86/assets/default.svg
    │   │   ├── https://content.cuvva.com/segments/39d789834ebeba3553a8aa133e5735d31a2796a84b87df9e08a59fe53cf12b86/assets/operations.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   ├── Stylesheets
    │   │   └── https://use.typekit.net/xcc2pjd.css
    │   └── /about
    │       ├── Images
    │       │   ├── /static/images/cuvva_logo.svg
    │       │   ├── https://www.cuvva.com/static/images/testimonials/testimonial-4.jpg
    │       │   ├── https://www.cuvva.com/static/images/testimonials/testimonial-1.jpg
    │       │   ├── https://www.cuvva.com/static/images/testimonials/testimonial-7.jpg
    │       │   ├── https://static.cuvva.com/svcs/staff/user_000000BkCGStslD6W7PFOQxJNixqC.jpg
    │       │   ├── https://static.cuvva.com/svcs/staff/92ff1593-c7e5-4ff8-856d-c07f1811085c.jpg
    │       │   ├── https://static.cuvva.com/svcs/doggos/doggo_000000Bg1oEB9bu0kynJeUCqJfZny.jpg
    │       │   ├── https://static.cuvva.com/svcs/staff/user_000000BlTlL8ejXM7B0IhURapcOci.jpg
    │       │   ├── https://static.cuvva.com/svcs/staff/328aa1c9-5207-43f2-9bd9-d86fc4913ae9.jpg
    │       │   ├── https://static.cuvva.com/svcs/staff/a169cd26-6043-4c5c-958d-d84209bb518c.jpg
    │       │   ├── https://static.cuvva.com/svcs/staff/user_000000BoyAcRC5d4Uybw7eiDix32m.jpg
    │       │   ├── https://static.cuvva.com/svcs/staff/user_000000Bbls2Dqkj10DvV7ELWU5o5A.jpg
    │       │   ├── https://static.cuvva.com/svcs/staff/user_000000BSwd87ffLzD3zJ29BXvQEzY.jpg
    │       │   ├── https://static.cuvva.com/svcs/staff/user_000000Be8KrKF5qzwRB3cEd5u0m6i.jpg
    │       │   ├── https://static.cuvva.com/svcs/staff/user_000000BaylHWi1yuBUR4Mu4lT3808.jpg
    │       │   ├── https://static.cuvva.com/svcs/staff/ec64ea64-ccf5-4586-a0bd-61b171cf80fe.jpg
    │       │   ├── https://static.cuvva.com/svcs/staff/53c34efa-95aa-476c-a244-971ee3ead9df.jpg
    │       │   ├── https://static.cuvva.com/svcs/staff/ab4bb578-1d25-4c81-b330-11dd0721bc25.jpg
    │       │   ├── https://static.cuvva.com/svcs/staff/user_000000BXdJl0tonn63VJzgWSulzAO.jpg
    │       │   ├── https://static.cuvva.com/svcs/staff/user_000000BiO5h1bcwRLkDRMtSOJWd72.jpg
    │       │   ├── /static/images/investors/breega.svg
    │       │   ├── /static/images/investors/fretlink.svg
    │       │   ├── /static/images/investors/curve.svg
    │       │   ├── /static/images/investors/tekton.svg
    │       │   ├── /static/images/investors/atrium.svg
    │       │   ├── /static/images/investors/sidecar.svg
    │       │   ├── /static/images/investors/localglobe.svg
    │       │   ├── /static/images/investors/citymapper.svg
    │       │   ├── /static/images/investors/robinhood.svg
    │       │   ├── /static/images/investors/seedcamp.svg
    │       │   ├── /static/images/investors/transferwise.svg
    │       │   ├── /static/images/investors/trussle.svg
    │       │   ├── /static/images/investors/Techstars.svg
    │       │   ├── /static/images/investors/uber.svg
    │       │   ├── /static/images/investors/pillpack.svg
    │       │   ├── /static/images/investors/breega.svg
    │       │   ├── /static/images/investors/fretlink.svg
    │       │   ├── /static/images/investors/curve.svg
    │       │   ├── /static/images/investors/tekton.svg
    │       │   ├── /static/images/investors/atrium.svg
    │       │   ├── /static/images/investors/sidecar.svg
    │       │   ├── /static/images/investors/localglobe.svg
    │       │   ├── /static/images/investors/citymapper.svg
    │       │   ├── /static/images/investors/robinhood.svg
    │       │   ├── /static/images/investors/seedcamp.svg
    │       │   ├── /static/images/investors/transferwise.svg
    │       │   ├── /static/images/investors/trussle.svg
    │       │   ├── /static/images/investors/Techstars.svg
    │       │   ├── /static/images/investors/uber.svg
    │       │   ├── /static/images/investors/pillpack.svg
    │       │   ├── /static/images/investors/breega.svg
    │       │   ├── /static/images/investors/fretlink.svg
    │       │   ├── /static/images/investors/curve.svg
    │       │   ├── /static/images/investors/tekton.svg
    │       │   ├── /static/images/investors/atrium.svg
    │       │   ├── /static/images/investors/sidecar.svg
    │       │   ├── /static/images/investors/localglobe.svg
    │       │   ├── /static/images/investors/citymapper.svg
    │       │   ├── /static/images/investors/robinhood.svg
    │       │   ├── /static/images/investors/seedcamp.svg
    │       │   ├── /static/images/investors/transferwise.svg
    │       │   ├── /static/images/investors/trussle.svg
    │       │   ├── /static/images/investors/Techstars.svg
    │       │   ├── /static/images/investors/uber.svg
    │       │   ├── /static/images/investors/pillpack.svg
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │       │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │       │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │       ├── Scripts
    │       │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │       │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │       │   └── /website.b2f26761.js
    │       └── Stylesheets
    │           └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works#tools
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── /static/design-system/illustrations/dog-says-hi.svg
    │   │   ├── /static/design-system/illustrations/new-car.svg
    │   │   ├── /static/design-system/illustrations/new-car.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /get-an-estimate?product=stm_learner
    │   ├── Images
    │   │   └── /static/images/cuvva_logo.svg
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /get-an-estimate?product=ltm
    │   ├── Images
    │   │   └── /static/images/cuvva_logo.svg
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /get-an-estimate?product=stm
    │   ├── Images
    │   │   └── /static/images/cuvva_logo.svg
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── https://www.cuvva.com/get-an-estimate?product=stm
    │   ├── Images
    │   │   └── /static/images/cuvva_logo.svg
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── https://www.cuvva.com/get-an-estimate
    │   ├── Images
    │   │   └── /static/images/cuvva_logo.svg
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works/what-to-look-for-when-buying-car-insurance
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works/types-of-car-insurance
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works/why-you-need-insurance-to-drive-a-new-car-home
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works/car-insurance-excess-explained
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works/can-you-insure-a-car-you-dont-own
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works/why-car-insurance-is-so-expensive
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works/write-offs
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works/imported-cars
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works/engine-size-and-insurance-price
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works/can-you-insure-a-car-without-an-mot
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works/transferring-your-insurance-to-another-car
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /car-insurance/temporary/drive-away
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── /static/images/landing/peoplewalking.svg
    │   │   ├── /static/design-system/illustrations/time.svg
    │   │   ├── /static/design-system/illustrations/product.svg
    │   │   ├── /static/design-system/illustrations/sanctus.svg
    │   │   ├── /static/design-system/illustrations/comprehensive.svg
    │   │   ├── /static/images/landing/phone.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works/job-titles-and-car-insurance
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works/insurance-group-2
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works/car-insurance-claims-explained
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works/young-driver-car-insurance-why-its-so-expensive
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works/how-long-does-a-car-insurance-claim-stay-on-your-record
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works/older-vs-newer-cars
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works/vehicle-value-and-price
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works/postcode-and-price
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works/insurance-group-3
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /how-insurance-works/insurance-group-1
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /support/can-i-view-policy
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /support/how-much-can-be-claimed-in-a-medical-emergency
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /support/extending-your-policy
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /support/how-do-i-cancel-a-policy
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /support/what-do-you-do-if-you-have-an-incident
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /support/get-quotes-purchase
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /support/not-offer-phone-support
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /category/the-app
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /support/continuous-insurance-enforcement-cie
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /support/how-does-our-pricing-work
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /category/motor
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /support/age-restrictions
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /category/data-security
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /category/regulatory-bodies-and-other-parties
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /support/motor-insurance-database-mid
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /support/how-is-cuvva-regulated
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /product-updates/how-we-analyse-and-test-new-pricing-models
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Product-.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /cuvva/lockdown-insurers-gains-should-be-returned-to-customers
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Insights-.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /news/weve-raised-15m-to-build-a-brand-new-kind-of-car-insurance
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Insights-.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /cuvva/the-future-of-cops
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Screenshot-2019-07-19-at-14.52.03.png
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Ops-dashboard-screenshot-1.jpg
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /cuvva/building-a-positive-data-culture
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /cuvva/how-the-cops-team-deals-with-system-outages
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /cuvva/how-do-our-product-teams-work-with-cops
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /product-updates/keeping-things-consistent-how-we-reworked-our-typeface
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Product-.png
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/image1.png
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/image2.png
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/image3.png
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/animation1.gif
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /car-insurance/temporary/insights/what-the-future-holds-for-the-car-sharing-economy
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Graph-4-no-title.jpg
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Graph-1.jpg
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Graph-2.jpg
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Graph-4.jpg
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Graph-3.jpg
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /cuvva/meet-anna-our-designer
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /cuvva/1-minute-response-time-24-7-365-days-a-year
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Image-from-iOS-14.jpg
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /cuvva/how-we-use-figma-to-empower-the-whole-company
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /cuvva/hello-food-bank-finder
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/food-bank-header.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /cuvva/why-we-sometimes-freeze-accounts
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── /support/cuvva-complaints-policy
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── https://www.cuvva.com/news/
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Graph-4-no-title.jpg
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Insights-.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/food-bank-header.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Insights-.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Product-.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Product-.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    ├── https://www.cuvva.com/cuvva/how-we-test-and-roll-out-new-product-features
    │   ├── Images
    │   │   ├── /static/images/cuvva_logo.svg
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Product-.png
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Christmas.png
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/ab-check.png
    │   │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/ab-check-in-app.png
    │   │   ├──
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
    │   │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
    │   │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
    │   ├── Scripts
    │   │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
    │   │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
    │   │   └── /website.b2f26761.js
    │   └── Stylesheets
    │       └── https://use.typekit.net/xcc2pjd.css
    └── https://www.cuvva.com/cuvva/transparency-and-the-role-of-ask-me-anything
        ├── Images
        │   ├── /static/images/cuvva_logo.svg
        │   ├── https://content.cuvva.com/segments/5d44eac4a0600027b4093586e4f00207f253fb8544fd63f15d140b0a19567779/assets/old-blog/Team.png
        │   ├──
        │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/financial-times.png
        │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/bloomberg.png
        │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/sky-news.png
        │   ├── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/techcrunch.png
        │   └── https://content.cuvva.com/segments/543d472ba1436c4ea6961beb6d5639a0fe4a901387c279efa5b07d579bd6fb99/assets/footer/the-times.png
        ├── Scripts
        │   ├── //widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js
        │   ├── https://www.googletagmanager.com/gtag/js?id=UA-59254430-1
        │   └── /website.b2f26761.js
        └── Stylesheets
            └── https://use.typekit.net/xcc2pjd.css
```
