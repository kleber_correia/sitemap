package fetcher

import (
	"context"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/klebervirgilio/sitemap/sitemap/pkg/pact"
)

type httpPageFetcher struct {
	domain string
}

func NewHTTPFetcher(domain string) pact.PageFetcher {
	return &httpPageFetcher{domain: domain}
}

func (c *httpPageFetcher) Fetch(_ context.Context, domain pact.FollowableDomain) ([]byte, error) {
	sdomain := string(domain)
	if strings.HasPrefix(sdomain, "/") {
		sdomain = c.domain + sdomain
	}

	resp, err := http.Get(sdomain)
	if err != nil {
		return nil, err
	}
	return ioutil.ReadAll(resp.Body)
}
