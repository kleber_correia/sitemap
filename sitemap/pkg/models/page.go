package models

// Page represents the page data
type Page struct {
	Domain      string
	Javascripts []string
	Stylesheets []string
	Images      []string
	Pages       []Page
}
