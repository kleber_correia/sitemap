package parsers

import (
	"bytes"
	"context"

	"github.com/antchfx/htmlquery"
	"github.com/klebervirgilio/sitemap/sitemap/pkg/models"
	"github.com/klebervirgilio/sitemap/sitemap/pkg/pact"
	"golang.org/x/net/html"
)

type HTMLParser struct {
}

func NewHTMLParser() pact.PageParser {
	return &HTMLParser{}
}

func (parser *HTMLParser) Parse(ctx context.Context, domain pact.FollowableDomain, HTTPBlob []byte) (*models.Page, error) {
	doc, err := htmlquery.Parse(bytes.NewReader(HTTPBlob))
	if err != nil {
		return nil, err
	}
	links, err := parser.findLinks(doc)
	if err != nil {
		return nil, err
	}

	page := models.Page{Domain: string(domain)}
	for _, link := range links {
		page.Pages = append(page.Pages, models.Page{Domain: link})
	}

	css, err := parser.findCss(doc)
	if err != nil {
		return nil, err
	}
	page.Stylesheets = css

	js, err := parser.findJS(doc)
	if err != nil {
		return nil, err
	}
	page.Javascripts = js

	imgs, err := parser.findImgs(doc)
	if err != nil {
		return nil, err
	}
	page.Images = imgs

	return &page, nil
}

func (parser *HTMLParser) findLinks(doc *html.Node) (domains []string, err error) {
	nodes, err := htmlquery.QueryAll(doc, "//a")
	if err != nil {
		return
	}
	for _, node := range nodes {
		for _, attr := range node.Attr {
			if attr.Key == "href" {
				domains = append(domains, attr.Val)
				break
			}
		}
	}
	return
}

func (parser *HTMLParser) findImgs(doc *html.Node) (imgs []string, err error) {
	nodes, err := htmlquery.QueryAll(doc, "//img")
	if err != nil {
		return
	}
	for _, node := range nodes {
		for _, attr := range node.Attr {
			if attr.Key == "src" {
				imgs = append(imgs, attr.Val)
				break
			}
		}
	}
	return
}

func (parser *HTMLParser) findJS(doc *html.Node) (js []string, err error) {
	nodes, err := htmlquery.QueryAll(doc, "//script")
	if err != nil {
		return
	}
	for _, node := range nodes {
		for _, attr := range node.Attr {
			if attr.Key == "src" {
				js = append(js, attr.Val)
				break
			}
		}
	}
	return
}

func (parser *HTMLParser) findCss(doc *html.Node) (css []string, err error) {
	nodes, err := htmlquery.QueryAll(doc, "//link")
	if err != nil {
		return
	}
	for _, node := range nodes {
		iscss := false
		for _, attr := range node.Attr {
			if attr.Key == "rel" && attr.Val == "stylesheet" {
				iscss = true
				break
			}
		}
		if iscss {
			for _, attr := range node.Attr {
				if attr.Key == "href" {
					css = append(css, attr.Val)
					break
				}
			}
		}
	}
	return
}
