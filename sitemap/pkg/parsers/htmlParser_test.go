package parsers_test

import (
	"context"
	"testing"

	"github.com/klebervirgilio/sitemap/sitemap/pkg/parsers"
)

var page = `<html><a href="followable-domain" /></html>`

func TestHTMLParser(t *testing.T) {
	parser := parsers.NewHTMLParser()
	page, err := parser.Parse(context.TODO(), "foo", []byte(page))
	if err != nil {
		t.Error(err)
	}
	if page.Domain != "foo" {
		t.Error("fail to assign domain")
	}
	if page.Pages[0].Domain != "followable-domain" {
		t.Error("fail to assign children domain")
	}
}
