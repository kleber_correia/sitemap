package service_test

import (
	"context"
	"log"
	"strings"
	"sync"
	"testing"

	"github.com/klebervirgilio/sitemap/sitemap/pkg/models"
	"github.com/klebervirgilio/sitemap/sitemap/pkg/pact"
	"github.com/klebervirgilio/sitemap/sitemap/pkg/service"
)

var (
	pageParent = models.Page{Domain: "kleber", Pages: []models.Page{pageChild, complexPage}}
	pageChild  = models.Page{Domain: "kleber1"}

	page6 = models.Page{Domain: "kleber6"}
	page7 = models.Page{Domain: "kleber7"}
	page5 = models.Page{Domain: "kleber5", Pages: []models.Page{page6, page7}}

	page9  = models.Page{Domain: "kleber9"}
	page10 = models.Page{Domain: "kleber10"}
	page8  = models.Page{Domain: "kleber8", Pages: []models.Page{page9, page10}}

	page3 = models.Page{Domain: "kleber3"}
	page4 = models.Page{Domain: "kleber4"}
	page2 = models.Page{
		Domain: "kleber2",
		Pages: []models.Page{
			page3,
			page4,
			page5,
			page8,
		},
	}

	allPages = map[string]models.Page{
		"kleber":   pageParent,
		"kleber0":  complexPage,
		"kleber1":  pageChild,
		"kleber2":  page2,
		"kleber3":  page3,
		"kleber4":  page4,
		"kleber5":  page5,
		"kleber6":  page6,
		"kleber8":  page8,
		"kleber7":  page7,
		"kleber9":  page9,
		"kleber10": page10,
	}

	complexPage = models.Page{
		Domain: "kleber0",
		Pages:  []models.Page{page2},
	}
	pages = []*models.Page{&complexPage, &pageChild}
)

type pageParser struct{}

func (f pageParser) Parse(_ context.Context, domain pact.FollowableDomain, blob []byte) (*models.Page, error) {
	page := allPages[string(blob)]
	return &page, nil
}

type pageFetcher struct{}

func (f pageFetcher) Fetch(_ context.Context, domain pact.FollowableDomain) ([]byte, error) {
	return []byte(string(domain)), nil
}

type kleberLinkFilter struct{}

func (f kleberLinkFilter) Filter(_ context.Context, domain pact.FollowableDomain) bool {
	return strings.Contains(string(domain), "kleber")
}

type consumer struct {
	sync.Mutex
	consumed []string
}

func (f *consumer) Consume(_ context.Context, parentDomain pact.FollowableDomain, page *models.Page) error {
	f.Lock()
	f.consumed = append(f.consumed, page.Domain)
	f.Unlock()
	return nil
}

var tLog func(args ...interface{})

func TestRunnerForPageWithGrandChildren(t *testing.T) {
	tLog = t.Log
	consumer := &consumer{}
	rnrCtx := service.RunnerCtx{
		PageFetcher: pageFetcher{},
		LinkFilter:  kleberLinkFilter{},
		Depth:       10,
		Consumer:    consumer,
		PageParser:  pageParser{},
	}
	runner := service.NewRunner(context.Background(), rnrCtx, log.Logger{})
	runner.Run("kleber")

	consumer.Lock()
	defer consumer.Unlock()
	for pageDomain, _ := range allPages {
		var found bool
		for _, consumedPage := range consumer.consumed {
			if pageDomain == consumedPage {
				found = true
				break
			}
		}
		if !found {
			t.Errorf("page %s not consumed", pageDomain)
		}
	}
}
