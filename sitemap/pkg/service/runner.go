package service

import (
	"context"
	"log"
	"sync"
	"time"

	"github.com/klebervirgilio/sitemap/sitemap/pkg/models"
	"github.com/klebervirgilio/sitemap/sitemap/pkg/pact"
)

type link struct {
	parent *models.Page
	depth  int
	url    pact.FollowableDomain
}

type Runner struct {
	sync.Mutex
	RunnerCtx
	chRun   chan link
	chDone  chan struct{}
	ctx     context.Context
	logger  log.Logger
	visited map[string]struct{}
}

type RunnerCtx struct {
	pact.PageFetcher
	pact.PageParser
	pact.LinkFilter
	pact.Consumer
	Depth int
}

func NewRunner(ctx context.Context, runnerCtx RunnerCtx, logger log.Logger) *Runner {
	return &Runner{
		ctx:       ctx,
		RunnerCtx: runnerCtx,
		logger:    logger,
		visited:   map[string]struct{}{},
		chRun:     make(chan link, 10),
		chDone:    make(chan struct{}, 1),
	}
}

func (runner *Runner) Run(domain string) {
	var wg sync.WaitGroup

	// give runner some warm up time
	time.AfterFunc(200*time.Millisecond, func() {
		wg.Wait()
		runner.Done()
	})

	// kicks runner off
	time.AfterFunc(100*time.Millisecond, func() {
		runner.chRun <- link{url: pact.FollowableDomain(domain)}
	})

	for {
		select {
		case link := <-runner.chRun:
			wg.Add(1)
			go func() {
				defer wg.Done()
				runner.pipeline(link)
			}()
		case <-runner.chDone:
			close(runner.chRun)
			return
		case <-time.After(1 * time.Minute):
			return
		case <-runner.ctx.Done():
			close(runner.chRun)
			return
		}
	}
}

func (runner *Runner) pipeline(link link) {
	if !runner.Filter(runner.ctx, link.url) {
		return
	}

	blob, err := runner.Fetch(runner.ctx, link.url)
	if err != nil {
		panic(err)
		return
	}

	page, err := runner.Parse(runner.ctx, link.url, blob)
	if err != nil {
		panic(err)
		return
	}

	var parentDomain string
	if link.parent != nil {
		parentDomain = link.parent.Domain
	}
	runner.Consume(runner.ctx, pact.FollowableDomain(parentDomain), page)
	runner.enqueue(link, page)
}

func (runner *Runner) Done() {
	runner.chDone <- struct{}{}
}

func (runner *Runner) enqueue(l link, page *models.Page) {
	if len(page.Pages) == 0 {
		return
	}

	if l.depth+1 >= runner.Depth {
		return
	}

	runner.Lock()
	defer runner.Unlock()

	for _, p := range page.Pages {
		if _, visited := runner.visited[string(p.Domain)]; visited {
			continue
		}

		runner.visited[string(p.Domain)] = struct{}{}
		runner.chRun <- link{
			parent: page,
			url:    pact.FollowableDomain(p.Domain),
			depth:  l.depth + 1,
		}
	}
}
