package repository_test

import (
	"context"
	"strings"
	"testing"

	"github.com/klebervirgilio/sitemap/sitemap/pkg/models"
	"github.com/klebervirgilio/sitemap/sitemap/pkg/repository"
)

func TestMemoryRepo(t *testing.T) {
	repo := repository.NewMemRepo()
	root := models.Page{Domain: "root.com", Images: []string{"img-0", "img-2"}}
	aboutUs := models.Page{Domain: "root.com/about-us", Images: []string{"img-4", "img-6"}}
	repo.Save(context.TODO(), "", &root)
	repo.Save(context.TODO(), root.Domain, &aboutUs)

	actual := repo.GetTree(context.TODO())

	if strings.Contains(actual, "root.com") && strings.Contains(actual, "root.com\\/about-us") &&
		strings.Contains(actual, "img-0") && strings.Contains(actual, "img-2") &&
		strings.Contains(actual, "img-6") && strings.Contains(actual, "img-4") {
		t.Errorf("malformed tree: %v", actual)
	}

}
