package repository

import (
	"context"
	"sync"

	"github.com/klebervirgilio/sitemap/sitemap/pkg/models"
	"github.com/klebervirgilio/sitemap/sitemap/pkg/pact"
	"github.com/xlab/treeprint"
)

type menRepository struct {
	sync.Mutex
	store *node
}

type node struct {
	page     *models.Page
	children []*node
}

func NewMemRepo() pact.Repository {
	return &menRepository{}
}

func (repo *menRepository) Save(ctx context.Context, parentDomain string, page *models.Page) error {
	if repo.store == nil {
		repo.store = &node{page: page, children: []*node{}}
		return nil
	}
	n := repo.findBy(parentDomain)
	n.children = append(n.children, &node{page: page, children: []*node{}})
	return nil
}

func (repo *menRepository) GetTree(ctx context.Context) string {
	var root treeprint.Tree
	tree := treeprint.New()
	currTree := tree

	queue := make([]*node, 0)
	queue = append(queue, repo.store)

	for len(queue) > 0 {
		nextUp := queue[0]
		queue = queue[1:]
		page := nextUp.page

		branch := currTree.AddBranch(page.Domain)
		if root == nil {
			root = branch
		}
		if len(page.Images) > 0 {
			imgs := branch.AddBranch("Images")
			for _, img := range page.Images {
				imgs.AddNode(img)
			}
		}

		if len(page.Javascripts) > 0 {
			jss := branch.AddBranch("Scripts")
			for _, js := range page.Javascripts {
				jss.AddNode(js)
			}
		}

		if len(page.Stylesheets) > 0 {
			css := branch.AddBranch("Stylesheets")
			for _, c := range page.Stylesheets {
				css.AddNode(c)
			}
		}

		if len(nextUp.children) > 0 {
			currTree = branch
			for _, child := range nextUp.children {
				queue = append(queue, child)
			}
			continue
		}
		currTree = root

	}
	return tree.String()
}

func (repo *menRepository) findBy(domain string) *node {
	queue := make([]*node, 0)
	queue = append(queue, repo.store)
	for len(queue) > 0 {
		nextUp := queue[0]
		queue = queue[1:]
		if nextUp.page != nil && nextUp.page.Domain == domain {
			return nextUp
		}
		if len(nextUp.children) > 0 {
			for _, child := range nextUp.children {
				queue = append(queue, child)
			}
		}
	}
	return nil
}
