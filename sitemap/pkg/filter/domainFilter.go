package filter

import (
	"context"
	"fmt"
	"regexp"
	"strings"

	"github.com/klebervirgilio/sitemap/sitemap/pkg/pact"
)

type filter struct {
	domain string
}

func NewDomainFilter(domain string) pact.LinkFilter {
	return &filter{domain}
}

func (f *filter) Filter(_ context.Context, domain pact.FollowableDomain) bool {
	sdomain := string(domain)
	if strings.HasPrefix(sdomain, "/") {
		return true
	}

	pattern := fmt.Sprintf("^(https?:\\/\\/)?%s.*", f.domain)
	b, _ := regexp.Match(pattern, []byte(sdomain))
	return b
}
