package pact

import (
	"context"

	"github.com/klebervirgilio/sitemap/sitemap/pkg/models"
)

// FollowableDomain specify domain-url type,  this is just to make our contract stronger and to leverage
// Go's type checking
type FollowableDomain string

// FetcherClient make sure HTML blobs fetchers can use any transport mechanism.
type FetcherClient interface {
	Do(domain FollowableDomain) ([]byte, error)
}

// PageFetcher uses fetcher client to retrieve HTML blob
type PageFetcher interface {
	Fetch(ctx context.Context, domain FollowableDomain) ([]byte, error)
}

// PageParser traverses HTML blob extracting static assets and links
type PageParser interface {
	Parse(ctx context.Context, domain FollowableDomain, HTTPBlob []byte) (*models.Page, error)
}

// LinkFilter filters followable links
type LinkFilter interface {
	Filter(ctx context.Context, domain FollowableDomain) bool
}

// Consumer persists the page/pages via Repository
type Consumer interface {
	Consume(ctx context.Context, parentDomain FollowableDomain, page *models.Page) error
}

// Repository controls how page's data in order to expose them so we can easily generate the output
type Repository interface {
	Save(ctx context.Context, parentDomain string, page *models.Page) error
	GetTree(ctx context.Context) string
}
