package consumer

import (
	"context"

	"github.com/klebervirgilio/sitemap/sitemap/pkg/models"
	"github.com/klebervirgilio/sitemap/sitemap/pkg/pact"
)

type consumer struct {
	pact.Repository
}

func NewConsumer(repo pact.Repository) pact.Consumer {
	return &consumer{Repository: repo}
}

func (c *consumer) Consume(ctx context.Context, parentDomain pact.FollowableDomain, page *models.Page) error {
	return c.Save(ctx, string(parentDomain), page)
}
