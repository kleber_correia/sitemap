package main

import (
	"context"
	"flag"
	"fmt"
	"log"

	"github.com/klebervirgilio/sitemap/sitemap/pkg/consumer"
	"github.com/klebervirgilio/sitemap/sitemap/pkg/fetcher"
	"github.com/klebervirgilio/sitemap/sitemap/pkg/filter"
	"github.com/klebervirgilio/sitemap/sitemap/pkg/parsers"
	"github.com/klebervirgilio/sitemap/sitemap/pkg/repository"
	"github.com/klebervirgilio/sitemap/sitemap/pkg/service"
)

func main() {
	var domain string
	var depth int
	flag.StringVar(&domain, "domain", "", "generate sitemap for domain")
	flag.IntVar(&depth, "depth", 2, "how deep should we go")
	flag.Parse()
	if domain == "" {
		fmt.Println("domain is required")
		return
	}

	repo := repository.NewMemRepo()
	consumer := consumer.NewConsumer(repo)
	htmlParser := parsers.NewHTMLParser()
	httpFetcher := fetcher.NewHTTPFetcher(domain)
	domainFilter := filter.NewDomainFilter(domain)

	runnerCtx := service.RunnerCtx{
		PageFetcher: httpFetcher,
		LinkFilter:  domainFilter,
		PageParser:  htmlParser,
		Consumer:    consumer,
		Depth:       depth,
	}
	ctx := context.Background()
	service.NewRunner(ctx, runnerCtx, log.Logger{}).Run(domain)
	fmt.Println(repo.GetTree(ctx))
}
